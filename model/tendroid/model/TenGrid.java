package tendroid.model;

public class TenGrid extends Grid {
	
	protected Grid grid[][];

	//Méthodes:

	public PositionList getGroup(Position p) {

		PositionList groupe=new PositionList();
		groupe = getGroupRec(p,groupe);
		return groupe;
	}

	private PositionList getGroupRec(Position p,PositionList connue) {
		if(p==null) return null;

		PositionList adj = adjPositions(p);
		for(int i=0;i<adj.size();i++) {
			if(g[adj.get(i).getCol()][adj.get(i).getLig()].equals(g[p.getCol()][p.getLig()])){
				if(!connue.contains(adj.get(i))) {
					connue.add(adj.get(i));
					connue = getGroupRec(adj.get(i), connue);
				}
			}
		}
		return connue;
	}

	public PositionList emptyPositions()
	{	//donne la liste des positions vides

		PositionList pList= new PositionList();
		for(int i=0;i<nbLig();i++){
			for(int j=0;j<nbCol();j++){
				if(get(new Position(j,i))==null){
					pList.add(j,i);
				}
			}
		}
		return pList;
	}
	
	public void collapseGroup(Position p) {
		if (!regularPosition(p)) return;
		PositionList groupe = getGroup(p);
		if(groupe==null) return;

		for(int i=0;i<groupe.size();i++) {
			if(!groupe.get(i).equals(p)){  // c'est la meme instance
				unset(groupe.get(i));
				//set(groupe.get(i),0);
			}
		}
		set(p,get(p)+1);
	}

	public void pack() {
		//compacte la grille
		 for (int k = 0; k < g.length; k++) {
		      for (int i = 0; i < g.length; i++) {
		         for (int j = g.length - 1; j > 0; j--) {
		            if (this.isEmpty(new Position(i, j))) {
		               g[i][j] = g[i][j - 1];
		               g[i][j - 1] = null;
		            }
		         }
		      }
		   }
		}
/*
		for (int j = 0; j <= nbCol(); j++) {
			for (int i = nbLig(); i == 0; i--) {
				Position tempo = new Position(j, i);
				Position next = new Position(j,i-1);
				if(get(tempo)==null && get(next)!=null){
					g[j][i]=g[j][i-1];
				}
			}
		}
	}*/
		

	public void refill(int[] ns) {
		
		
		//rempli les cases vides par les valeurs de ns

		//cas ou le tableau ne contient pas assez de valeur
		PositionList postList = emptyPositions();
		if(ns.length<postList.size()) return;
		for (int i = 0; i < postList.size(); i++) {
			super.set(postList.get(i), ns[i]);
		}
	}

	//Constructeur:
	public TenGrid() {
			super(5, 5);
		}
	public TenGrid( int[] ns){
			super(5, 5);
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					g[j][i]=ns[i*5+j];
				}
			}
		}

	}


