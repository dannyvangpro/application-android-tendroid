package tendroid.model;

public class TenGame extends TenGrid {
    public PositionList groupeSelec=null;
    public int ns[];

    //Constructeurs:
    public TenGame(){
        super();
    }
    public TenGame(int[] ns){
        super(ns);
    }

    //Méthodes:
    public PositionList getSelectedGroup()
    {   //donne la liste des positions du groupe selectionné

        return groupeSelec;
    }

    public void transition(Position p)
    {   //effectue l'une des 3 actions

        if(regularPosition(p)==false) groupeSelec=null;
        if(groupeSelec==null) {
            groupeSelec=getGroup(p);
            return;
        }
        if(groupeSelec.contains(p)) {
            collapseGroup(p);
            pack();
           
            refill(ns);
            return;
        }else{
            groupeSelec=getGroup(p);
        }
    }
  
}

    