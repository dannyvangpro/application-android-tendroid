package tendroid.model;

public class Position {
	private int col;
	private int lig;

	//Constructeur
	public Position(int col, int lig) {
		//crée une position avec une colonne et une ligne donner

		this.col = col;
		this.lig = lig;
	}

	//Méthodes
	public int getCol()
	{	//renvoie la colonne

		return col;
	}
	
	public int getLig()
	{//renvoie la ligne

		return lig;
	}

	public boolean equals(Position p)
	{	//calcule si les valeurs des positions sont les memes

		return this.col == p.col && this.lig == p.lig;
	}
	public String toString() {
		//affiche p avec sa position et sa ligne
		return "("+col+","+lig+")";

	}
}
