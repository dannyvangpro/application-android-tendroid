package tendroid.model;

public class Grid {
	protected Integer[][] g;
	protected int nbCol;
	protected int nbLig;

	//Constructeurs:
	public Grid(int nbCol, int nbLig ) {
		this.nbCol = nbCol;
		this.nbLig = nbLig;
		g = new Integer[nbCol][nbLig];
	}

	//Méthodes:
	public int nbCol()
	{	//retourne le nombre de colonne

		return nbCol;
	}
	
	public int nbLig()
	{	//retourne le nombre de ligne

		return nbLig;
	}
	
	public PositionList allPositions()
	{	//retourne toute les positions du tableau g

		PositionList allPos = new PositionList();
		
		for(int i = 0; i < nbCol; i++) {
			for(int j = 0; j < nbLig; j++) {
				allPos.add(i,j);
			}
		}
		return allPos;
	}
	
	public boolean regularPosition(Position p)
	{	//retourne true si la position p est dans la grille, false sinon

		if( (p.getCol()<nbCol && p.getCol()>=0) && (p.getLig()<nbLig && p.getLig()>=0) ) {
			return true;
		}
		return false;
	}
	
	public boolean isEmpty(Position p)
	{	//retourne true si la position p est dans la grille et que p est null, false sinon

		if(regularPosition(p) && g[p.getCol()][p.getLig()]==null) return true;
		 
		return false;
	}
		
	public Integer get(Position p)
	{	//retourne la valeur de p si il est dans la grille

		if (!regularPosition(p)) return Integer.parseInt(null);

		return g[p.getCol()][p.getLig()];

	}
	
	public void set(Position p, Integer v)
	{	//place la valeur v dans p s

		//cas ou p n'est pas une position de la grille
		if (!regularPosition(p)) {
			return;
		}
		g[p.getCol()][p.getLig()] = v;
	}
	
	public void unset(Position p)
	{	//rend vide la case p

		//cas ou p n'est pas une position de la grille
		if (!regularPosition(p)) {
			return;
		}
		g[p.getCol()][p.getLig()]=null;
	}
	
	public PositionList adjPositions(Position p)
	{	//donne la liste des positions adjacentes (récursion)

		PositionList posL=new PositionList();

		if (regularPosition(p)==false){
			return posL;
		}

		//cas pour droite
		if(p.getCol()+1<nbCol) {
			posL.add(new Position(p.getCol() + 1, p.getLig()));
		}
		//cas pour bas
		if(p.getLig()+1<nbLig){
			posL.add( new Position(p.getCol(), p.getLig()+1));}

		
		//cas pour gauche
		if(p.getCol()-1>=0){
			posL.add(  new Position(p.getCol()-1, p.getLig()));}

		//cas pour haut
		if(p.getLig()-1>=0){
			posL.add( new Position(p.getCol(), p.getLig()-1));}

		return posL;
	}
	public String toString() {
		String s="";
		for(int i=0;i<5;i++) {
			for(int j=0;j<5;j++) {
				s+=get(new Position(j,i))+" ";
			}
			s+="\n";
		}

		return s;
	}
}
