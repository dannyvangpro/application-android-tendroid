package tendroid.model;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class PositionList extends ArrayList<Position>{

	//Constructeur
	public PositionList() {
		super();
	}

	//Méthodes
	public boolean add(int col, int lig)
	{	//rajoute une position dans le tableau pos

		return this.add(new Position(col,lig));
	}
	
	public boolean contains(Position p)
	{	//retourne true si il existe une position egale a p, false sinon
		for(int i=0; i<size();i++){
			if(get(i)==p || get(i).equals(p)) {
				return true;
			}
		}
		return false;	
	}
	
	public String toString() {
		String s="";
		if(size()==0) return "Liste vide";
		for(int i=0;i<size();i++) {
			s+= get(i).toString()+" ";
		}

		return s;
	}
}
