package tendroid.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

public class PlayActivity extends Activity{
    TheApplication app;
    TextView score;
    private Switch myswitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if(AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.dark);
        }else
            setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_activity);


        app = (TheApplication)(this.getApplication());
        score= (TextView) findViewById(R.id.Score);
        score.setText("Score : "+ app.score );
        if (app.trouve==1){
            Intent other= new Intent(getApplicationContext(),PlayActivity.class);
            startActivity(other);
        }

        myswitch=(Switch)findViewById(R.id.myswitch);
        if (AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){
            myswitch.setChecked(true);
        }

        myswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    restartApp();
                }else{
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    restartApp();
                }
            }
        });
        app = (TheApplication)(this.getApplication());
        score= (TextView) findViewById(R.id.Score);
        score.setText("Score : "+ app.score );
        if (app.trouve==1){
            Intent other= new Intent(getApplicationContext(),victory.class);
            startActivity(other);
        }
    }

    public void restartApp(){
        Intent i = new Intent(getApplicationContext(),PlayActivity.class);
        startActivity(i);
        finish();
    }

    public void retour(View v) {
        finish();
    }

    public void reset(View view){
        /*app.getGame().reNew();
        startActivity(new Intent(getApplicationContext(),PlayActivity.class));*/
        app.reNew();
        ((GameBoard)findViewById(R.id.gameBoard)).reDraw();
    }

    protected void Victory(Bundle savedInstanceState) {
        app.gameOver();
        setContentView(R.layout.victory);
    }

    public void affiche_score(){

    }
}
