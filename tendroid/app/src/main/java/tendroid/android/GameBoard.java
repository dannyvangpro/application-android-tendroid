package tendroid.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import android.os.Bundle;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;

import java.util.ListIterator;
import tendroid.model.Position;
import tendroid.model.TenGame;
import tendroid.android.TheApplication;

/**
 * Created by 3670050 on 27/02/19.
 */

public class GameBoard extends SurfaceView implements SurfaceHolder.Callback {
    Position p;
    TheApplication app;
    Paint paint = new Paint();
    int canvasWidth;
    int cellSize;
    int click;
    TextView score;

    public GameBoard(Context c) {
        super(c);
        getHolder().addCallback(this);
        getApp(c);
    }

    public GameBoard(Context c, AttributeSet as) {
        super(c, as);
        getHolder().addCallback(this);
        getApp(c);
    }

    @Override
    public void onDraw(Canvas canvas) {

        paint.reset();
        TenGame theGame = app.getGame();
        int trouve=0;
        //groupe selectioner + pack
        if(click==2) {
            app.score++;
            theGame.groupeSelec = null;
            //theGame.ns=null;
            click=0;

        }
        for (int x = 0; x < 5; x++) {
            for (int y = 0; y < 5; y++) {
                if (app.m.get(new Position(x,y)) == 10) {
                    trouve = 1;
                }
            }
        }
        if(trouve==1) canvas.drawColor(Color.YELLOW);
        else canvas.drawColor(Color.GRAY);

        //lignes verticales
        paint.setColor(Color.BLACK);
        for (int x = 0; x < canvasWidth; x += cellSize) {
            canvas.drawLine(x, 0, x, canvasWidth, paint);
        }
        //lignes horizontales
        paint.setColor(Color.BLACK);
        for (int y = 0; y < canvasWidth; y += cellSize) {
            canvas.drawLine(0, y, canvasWidth, y, paint);
        }

        String[] tab={"#ffff66" ,"#ffff66","#FFCC00","#FF9900","#FF6600","#FF3300","#FF0000","#660099","#660066","#660033","#660000"};
        for (int i=0; i<theGame.nbCol();i++){
            for(int j=0; j<theGame.nbLig();j++){
                Position p=new Position(i,j);
                paint.setColor(Color.parseColor(tab[theGame.get(p)]));
                canvas.drawRect(p.getCol()*cellSize,p.getLig()*cellSize,p.getCol()*cellSize+cellSize,p.getLig()*cellSize+cellSize,paint);
            }
        }

        //couleurs tous les chiffres
        paint.setColor(Color.BLUE);
        paint.setTextSize(100); // Constante
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        for (int x = 0; x < 5; x++) {
            for (int y = 0; y < 5; y++) {
                paint.setColor(Color.GRAY);
                canvas.drawText(Integer.toString(theGame.get(new Position(x, y))),
                        (x * cellSize) + 11,
                        (cellSize + y * cellSize) - 6,
                        paint);
                if (theGame.get(new Position(x, y))== (int) 10) app.trouve=1;

                if (theGame.getSelectedGroup() != null ) {
                    paint.setColor(Color.GREEN);
                    for (int i = 0; i < theGame.getSelectedGroup().size(); i++) {
                        canvas.drawText(Integer.toString(theGame.get(theGame.getSelectedGroup().get(i))),
                                (theGame.getSelectedGroup().get(i).getCol() * cellSize) + 11,
                                (cellSize + theGame.getSelectedGroup().get(i).getLig() * cellSize) - 6, paint);
                    }
                }
            }
        }

    }


        public void reDraw() {
        TenGame theGame = app.getGame();
        Canvas c = getHolder().lockCanvas();
        if (c != null) {
            this.onDraw(c);
            getHolder().unlockCanvasAndPost(c);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {reDraw(); }
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        canvasWidth = width;
        cellSize = width/5; // ATTENTION: constante
        reDraw();
    }
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {  }
    @Override

    public boolean onTouchEvent(MotionEvent event) {
        TenGame theGame = app.getGame();
        int x = (int) event.getX()*(theGame.nbCol())/canvasWidth;
        int y = (int) event.getY()*(theGame.nbLig())/canvasWidth;
        int action = event.getAction();


        switch (action) {
            case MotionEvent. ACTION_DOWN: {
                theGame.transition(new Position(x,y));
                reDraw();
                return true;
            }

            case MotionEvent. ACTION_UP: {
                click++;
                reDraw();
                return false;

            }

            default:
                return false;
        }
    }

    final void getApp(Context context) {
        app = (TheApplication) (context.getApplicationContext());
    }

}

