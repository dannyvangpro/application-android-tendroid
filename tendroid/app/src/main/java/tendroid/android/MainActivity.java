package tendroid.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void finish(View v){
        finish();
    }

    public void setting(View v) {
        Intent other = new Intent(this, setting.class);
        startActivity(other);
    }
    public void play(View v){
        Intent other = new Intent(this,PlayActivity.class);
        startActivity(other);
    }


}
